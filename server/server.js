/**
 * Created by vainio6 on 09/02/16.
 */

var bodyParser = require('body-parser');
var express = require('express');
var cors = require('cors');
var Promise = require('bluebird')

// Web socket connections
var WebSocketServer = require('ws').server;


var app = express();

var server = require('http').createServer(app);

app.use(cors());
app.use(bodyParser.json());

var env = app.get('env') == 'development' ? 'dev' : app.get('env');
var port = process.env.PORT || 3333;


// IMPORT MODELS
// =============================================================================
var Sequelize = require('sequelize');

// db config
var env = "dev";
var config = require('./database.json')[env];
var password = config.password ? config.password : null;

// initialize database connection
var sequelize = new Sequelize(
    config.database,
    config.user,
    config.password,
    {
        logging: console.log,
        define: {
            timestamps: false
        }
    }
);

var DataTypes = require("sequelize");

var Details = sequelize.define('details', {

    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    product_id: DataTypes.INTEGER,
    dkey: DataTypes.STRING,
    value: DataTypes.STRING
}, {
    instanceMethods: {

        updateByBulk: function(id, onSuccess, onError) {

            Promise.all(
                this.details.map(function(detail) {
                    if (detail.id)
                        return Details.update(detail, {where: {id: detail.id}});
                    else {
                        detail.product_id = id;
                        return Details.create(detail);
                    }

                })
            ).then(onSuccess).error(onError);
        },
        deleteById: function(id, onSuccess, onError) {
            Details.destroy({where:{id: id}})
                .then(onSuccess).error(onError);
        }
    },
        underscored : true

});


var Products = sequelize.define('products', {

    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    name: DataTypes.STRING,
    category: DataTypes.STRING,
    code: DataTypes.STRING,
    price: DataTypes.FLOAT
},{
    underscored : true,
    instanceMethods: {
        retrieveById: function(id, onSuccess, onError) {
            Products.find({where: {id: id},
                    include: [
                        { model: Details }
                    ], raw:false})
                .then(onSuccess).error(onError);
        },
        deleteById: function(id, onSuccess, onError) {
            Products.destroy({where:{id: id}, include: [{ model: Details }]})
                .then(onSuccess).error(onError);
        },
        retrieveAll: function(onSuccess, onError) {
            Products.findAll({include: [{ model: Details }], raw:false})
                .then(onSuccess).error(onError);
        },
        updateById: function(id, onSuccess, onError){
            console.log(this.receivedBody);
            Products.update(this.receivedBody, {where: {id: id},
                include: [
                    { model: Details }
                ]
            })
                    .then(onSuccess).error(onError);
        },
        create: function(onSuccess, onError){
            Products.create(this.receivedBody, {include: [{ model: Details }]})
                .then(onSuccess).error(onError);
        }
    }


});

Products.hasMany(Details);
Details.belongsTo(Products, { as: 'product', constraints: false })

sequelize.sync();

// IMPORT ROUTES
// =============================================================================
var router = express.Router();

// on routes that end in /products
// ----------------------------------------------------
router.route('/products')
    // get all products accessed at GET http://localhost:8080/products)
    .get(function(req, res) {
        var product = Products.build();

        product.retrieveAll(function(products) {
            if (products) {
                res.json(products);
            } else {
                res.send(401, "User not found");
            }
        }, function(error) {
            res.send("User not found");
        });
    })

    .post(function(req, res) {
        var product = Products.build();

        product.receivedBody = req.body;
        product.create(function(success) {
            if (success) {
                res.json({ message: 'Product created!' });
            } else {
                res.send(401, "Product not found");
            }
        }, function(error) {
            res.send("Product not found");
        });
    });

router.route('/details/:id')
    .delete(function(req, res) {
        var details = Details.build();

        details.deleteById(req.params.id, function(details) {
            if (details) {
                res.json(details);
            } else {
                res.send(401, "User not found");
            }
        }, function(error) {
            res.send("User not found");
        });
    });

// on routes that end in /products/:id
// ----------------------------------------------------
router.route('/products/:id')

// get a product by id(accessed at GET http://localhost:8080/products/:id)
.get(function(req, res) {
    var product = Products.build();

    product.retrieveById(req.params.id, function(products) {
        if (products) {
            res.json(products);
        } else {
            res.send(401, "User not found");
        }
    }, function(error) {
        res.send("User not found");
    });
})
.delete(function(req, res) {
    var product = Products.build();

    product.deleteById(req.params.id, function(products) {
        if (products) {
            res.json(products);
        } else {
            res.send(401, "User not found");
        }
    }, function(error) {
        res.send("User not found");
    });
})
.put(function(req, res) {
    var product = Products.build();
    var details = Details.build();

    product.receivedBody = req.body;
    details.details = req.body.details;
    product.updateById(req.params.id, function(success) {
        if (success) {

            details.updateByBulk(req.params.id, function(success) {
                if (success) {

                    res.json({ message: 'Product updated!' });
                } else {
                    res.send(401, "Product not found");
                }
            }, function(error) {
                res.send("Product not found");
            });

        } else {
            res.send(401, "Product not found");
        }
    }, function(error) {
        res.send("Product not found");
    });
});

// REGISTER OUR ROUTES
// =============================================================================
app.use('/', router);





var wsServer = require('ws').Server({server: server, perMessageDeflate: false});

/*wsServer = new WebSocketServer({
    httpServer: app
});*/

var count = 0;
var clients = {};

wsServer.on('connection', function(ws){

    // Specific id for this client & increment count
    var id = count++;
// Store the connection method so we can loop through & contact all clients
    clients[id] = ws;
    console.log((new Date()) + ' Connection accepted [' + id + ']');
    ws.send('{"welcome":"ok"}');

    for(var i in clients){
        // Send a message to the client with the message
        clients[i].send('{"online": ' + Object.keys(clients).length +' }');
    }

    // Create event listener
    ws.on('message', function(message) {

        // The string message that was sent to us
        var msgString = message.utf8Data;

        // Loop through all clients
        for(var i in clients){
            // Send a message to the client with the message
            clients[i].send(msgString);
        }

    });

    ws.on('close', function(reasonCode, description) {

        console.log((new Date()) + ' Peer ' + clients[id].upgradeReq.connection.remoteAddress + ' disconnected.');

        delete clients[id];

        for(var i in clients){
            // Send a message to the client with the message
            clients[i].send('{"online": ' + Object.keys(clients).length +' }');
        }

    });

});

wsServer.on('error', function(err) {
    util.log("Websocket server error: " + util.inspect(err));
});

// START THE SERVER
// =============================================================================
server.listen(port);
console.log('Listening ' + port);
