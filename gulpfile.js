/**
 * Created by vainio6 on 11/01/16.
 */

/* Project dependent constants */
var mainModuleName = "CanterApp";

var gulp = require("gulp");
var browserify = require("browserify");
var watchify = require("watchify");
var source = require("vinyl-source-stream");
var buffer = require('gulp-buffer');
var sourcemaps = require('gulp-sourcemaps');
var browserSync = require("browser-sync").create();
var uglify = require('gulp-uglify');
var ngAnnotate = require('gulp-ng-annotate');
var templateCache = require('gulp-angular-templatecache');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyCSS = require('gulp-minify-css');
var autoprefixer = require('gulp-autoprefixer');
var notify = require('gulp-notify');
var ngConstant = require('gulp-ng-constant');
var del = require('del');

var packageJson = require('./package.json');
var dependencies = Object.keys(packageJson && packageJson.dependencies || {});

var environmentConfig = require('./environmentConfig.json');

gulp.task('serve', ['watch-all'], function(){

    browserSync.init(["./dist/*.js", "./dist/*.css", "./dist/**/*.html"], {
        server: "./dist",
        ghostMode: false,
        logFileChanges: false
    });

    gulp.watch("./dist/*.html").on('change', browserSync.reload);
    gulp.watch("./dist/*.css").on('change', browserSync.reload);
    gulp.watch("./dist/*.js").on('change', browserSync.reload);


});

gulp.task("clean", function(){
    return del(['./dist', './tmp'])
});

gulp.task('watch-all', ['watch-sass', 'watch-assets', 'watch-index', 'watch-html', 'watch-vendor', 'watch-config', 'watch-app']);
gulp.task('build-all', ['build-css-from-sass', 'build-assets', 'build-index', 'build-html', 'build-vendor', 'build-config', 'build-app']);
gulp.task('default', ['build-all']);


/****************************************************/
/* Non-independent tasks                            */
/****************************************************/
/* App bundle to include all own js files, it depends of dynamically generated angular file */
gulp.task('build-app', ['build-config'], function () {
    browserify({
        entries: ['./client/app.js'],
        cache: {},
        packageCache: {}
    })
        .external(dependencies)
        .bundle()
        .on("error", function (e) {
            console.log(e);
        })
        .pipe(source("app.js"))
        .pipe(buffer())
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest("./dist/"));
});

gulp.task('watch-app', ['build-app'], function () {
    return gulp.watch('./client/**/*.js', ['build-app']);
});

/****************************************************/
/* Independent tasks, which can be run concurrently */
/****************************************************/

/*------------------------------------------*/
/* Vendor files, (Package.json dependencies */
/*------------------------------------------*/
gulp.task('build-vendor', function () {
    return browserify({
        cache: {},
        packageCache: {}
    })
        .require(dependencies)
        .bundle()
        .pipe(source('vendor.js'))
        .pipe(buffer())
        .pipe(ngAnnotate())
        .pipe(uglify())
        .pipe(gulp.dest('./dist/'));
});

gulp.task('watch-vendor', ['build-vendor'], function () {
    return gulp.watch('package.json', ['build-vendor']);
});

/*--------------------------*/
/* Handle Sass files to css */
/*--------------------------*/
gulp.task('build-css-from-sass', function() {
    var autoprefixerOptions = {
        browsers: ['last 2 versions', '> 5%', 'Firefox ESR']
    };
    return gulp.src('./client/**/*.scss')
        .pipe(concat('style.min.css'))
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(minifyCSS())
        .pipe(autoprefixer(autoprefixerOptions))
        //        .pipe(cachebust.resources())
        .pipe(sourcemaps.write('./maps'))
        .pipe(gulp.dest('./dist'))
        .pipe(browserSync.stream({match: "style.min.css"}));
        //.pipe(notify({message: 'Styles task complete'}));

});

gulp.task('watch-sass', ['build-css-from-sass'], function () {
    return gulp.watch('./client/**/*.scss', ['build-css-from-sass']);
});

/*--------------------------------------------------------------------------------------*/
/* Favicons, images and other assets, now just copy to dist without optimization or compression */
/*--------------------------------------------------------------------------------------*/
gulp.task('build-assets', function() {
    return gulp.src('./client/assets/**/*')
        .pipe(gulp.dest('./dist/assets'));
});

gulp.task('watch-assets', ['build-assets'], function () {
    return gulp.watch('./client/assets/**/*', ['build-assets']);
});

/*--------------------------------------------------------------------*/
/* Index file, handled separately from other html (angular templates) */
/*--------------------------------------------------------------------*/
gulp.task('build-index', function() {
    return gulp.src('./client/index.html')
        .pipe(gulp.dest('./dist'));
});

gulp.task('watch-index', ['build-index'], function () {
    return gulp.watch('./client/index.html', ['build-index']);
});

/*------------------------*/
/* Angular html templates */
/*------------------------*/
gulp.task('build-html', function() {
    return gulp.src(['./client/**/*.html', '!./client/index.html'])
        .pipe(templateCache({moduleSystem: "Browserify"}))
        .pipe(gulp.dest('./client/templates'));
    //   .pipe(notify({ message: 'HTML task complete' }));
});

gulp.task('watch-html', ['build-html'], function () {
    return gulp.watch(['./client/**/*.html', '!./client/index.html'], ['build-html']);
});

/*---------------------------------------*/
/* Dynamically generated Angular js file */
/*---------------------------------------*/
gulp.task('build-config', function () {
    gulp.src('./angularConfig.json')
        .pipe(ngConstant({
            name: mainModuleName,
            deps: false,
            constants: { environment: environmentConfig },
            wrap: 'commonjs'
        }))
        .on("error", function(){process.exit(-1)})
        .pipe(gulp.dest('./client'));
});

gulp.task('watch-config', ['build-config'], function () {
    return gulp.watch('./environmentConfig.json', ['build-config']);
});
