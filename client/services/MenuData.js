/**
 * Created by vainio6 on 05/02/16.
 * Contains header menu selection as a service to be available to all views
 * Also light/dark theme is changed using this menudata
 */
'use strict';

module.exports = function () {

    var menu = [
        {title:"List", link:"#/productList", selected: true},
        {title:"Create", link:"#/productView", selected: false}
    ];
    var showSpinner = false;

    return {
        toggleSpinner: function(){
            showSpinner = !showSpinner;
        },
        showSpinner: function() {
            return showSpinner;
        },
        getMenu: menu,
        setItem: function(title){
            menu.forEach(function(item){
                item.selected = (title == item.title);
            });
        }
    }
};
