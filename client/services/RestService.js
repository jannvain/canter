/**
 * Created by vainio6 on 05/02/16.
 * Handles all Websocket communication using angular-websocket module
 */

'use strict';

module.exports = function($http, $log, $websocket) {

    var dataStream=null;
    var webServer=null;
    var listeners = [];
    var connected = false;

    var init = function() {
        connected = true;

        dataStream.onMessage(function (message) {

                try {
                    var message = JSON.parse(message.data);

                    if (message != null) {
                        listeners.forEach(function(listener){
                            listener(message);
                        });
                    }
                }
                catch(err){
                    // First message in socket is welcome message and not JSON, so just ignore it
                    // Server should correct it to a proper JSON.
                }
        });

        dataStream.onOpen(function (data) {
            connected = true;
        });
        dataStream.onClose(function (data) {
            connected = false;
        });
        dataStream.onError(function (data) {
            $log.debug("Error in stream " + data);
            connected = false;
        });

    }

    // Return the API
    var methods = {
        openConnection: function(server){
            webServer = server;
            if(dataStream === null)
                dataStream = $websocket('ws://' + server);
            else
                $log.debug("WEBSOCKET ALREADY OPEN");

            if(dataStream !== null) {
                init();
                $log.debug("SUCCESS OF OPENING SOCKET: " + server);
            }
            else
                $log.debug("ERROR IN WEBSOCKET");
        },
        isConnected: function() {
            return connected;
        },
        addSocketListener: function(listenCallback){
            listeners.push(listenCallback);
        },
        getProduct: function(id){
            var promise = $http.get('http://' + webServer + '/products/' + id).then(function (response){
                return response.data;
            });
            return promise;
        },
        deleteProduct: function(id){
            var promise = $http.delete('http://' + webServer + '/products/' + id).then(function (response){
                return response.data;
            });
            return promise;
        },
        deleteDetail: function(id){
            var promise = $http.delete('http://' + webServer + '/details/' + id).then(function (response){
                return response.data;
            });
            return promise;
        },
        getProductList: function(){
            var promise = $http.get('http://' + webServer + '/products').then(function (response){
                    return response.data;
                });
            return promise;
        },
        CreateOrUpdateProduct: function(product){

            var promise;
            if(product.id){ // Updating existing product
                $log.debug("UPDATING");
                promise = $http.put('http://' + webServer + '/products/' + product.id, product).then(function (resp) {
                    $log.debug("OK");
                    $log.debug(resp);
                });
            }
            else { // Otherwise creating a new product
                $log.debug("CREATING");
                promise = $http.post('http://' + webServer + '/products', product).then(function (resp) {
                    $log.debug("OK");
                    $log.debug(resp);
                });
            }
            return promise;
        }
    };

    return methods;
};


