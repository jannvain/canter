/**
 * Created by vainio6 on 12/01/16.
 */

'use strict';


// Initialized socket connection and receive messages about online users
// Number of online users is displayed in header

module.exports = function(menuData, environment, RestService){

    var scope = this;

    this.onlineUsers = 0;
    this.menu = menuData.getMenu;
    this.setItem = menuData.setItem;

    this.showSpinner = function(){
        return menuData.showSpinner();
    }
    this.updateOnlineUsers = function(msg){
        if(msg.online)
            scope.onlineUsers = msg.online;
    };

    this.initView = function() {
        RestService.addSocketListener(scope.updateOnlineUsers);
        RestService.openConnection(environment.server + ':' + environment.port);
    }

    this.initView();

};