/**
 * Created by vainio6 on 08/02/16.
 */

'use strict';

module.exports = function(RestService, menuData, $timeout) {

    var scope = this;

    menuData.setItem("List");

    this.productList = [];
    this.askDelete = false;
    this.deleted = false;
    this.deleteId = null;

    this.timeoutCallBack = function(){
        scope.deleteId = null;
        scope.initView();
        scope.askDelete = false;
    }

    this.reallyDelete = function() {
        RestService.deleteProduct(scope.deleteId).then(function(data){
            scope.deleted = true;
            $timeout(scope.timeoutCallBack, 2000);
        });
    }

    this.deleteProduct = function(id){
        scope.deleteId = id;
        scope.deleted = false;
        scope.askDelete = true;
    }

    this.initView = function(){
        menuData.toggleSpinner();

        RestService.getProductList().then(function(data){
            scope.productList = data;
            menuData.toggleSpinner();

        });
    }

    this.initView()
};