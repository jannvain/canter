/* Application main entry point, using CommonJS-modules and require to build the final app.js */

// Frontend is mainly build with Angular, aided by different angular modules
var angular = require('angular');
require('angular-route');
require('angular-websocket');
require('ng-lodash');
require('angularjs-scroll-glue');

// Dynamically created templates.js (gulp build-task is creating this)
// Containing all html-templates for angular views and includes, simplifies the distributed app
var templates = angular.module('templates',[]);
require('./templates/templates');

// All functionality is in a single Angular module CanterApp
var app = angular.module('CanterApp', [ 'ngRoute', 'ngLodash', 'templates', 'angular-websocket', 'luegg.directives']);

// Dynamically create Angular constant to contain configurable settings for server adresses, ports etc
// To NOT to hardwire those in actual code, Gulp build task is creating this js-file
require('./angularConfig');

// Handles just header menu and displaying the logo
app.controller('HeaderCtrl', require ('./header/header.controller'));

// Includes productlist logic
app.controller('ProductListCtrl', require('./productList/productList.controller'));

// Includes detailed product viewer/editor
app.controller('ProductViewCtrl', require('./productView/productView.controller'));

// The singleton service for Websocket AND rest-calls, could be split to
// separate rest and socket services in the future
app.service('RestService', require ('./services/RestService'));

// Directive to get input boxes to react for ENTER-keys and to autofocus, not used  now
//app.directive('myEnter', require('./directives/enter.directive'));
//app.directive('myFocus', require('./directives/myFocus.directive'));

// Main controller, currently not doing very much
app.controller('AppCtrl', function(){

    var scope = this;
    this.companyTitle = "Canter Oy";
    this.appName = "Product editor";
});

// A factory to contain header menu and status,  its menu providing routes for the router below
app.factory('menuData', require('./services/MenuData'));

app.config(function($routeProvider) {
    $routeProvider.when('/productList', {
            templateUrl: 'productList/productList.html',
            controller: 'ProductListCtrl',
            controllerAs: 'list'
        })
        .when('/productView/:id', {
            templateUrl: 'productView/productView.html',
            controller: 'ProductViewCtrl',
            controllerAs: 'view'
        })
        .when('/productView', {
            templateUrl: 'productView/productView.html',
            controller: 'ProductViewCtrl',
            controllerAs: 'view'
        })
        .otherwise({
            redirectTo: '/productList'
        });
});
