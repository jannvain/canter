
module.exports = angular.module("CanterApp")

.constant("environment", {
	"NOTE": "This is dynamically created angular file, do not edit directly, use environmentConfig.json instead!!!",
	"type": "development",
	"server2": "192.168.11.6",
	"server": "localhost",
	"port": "3333",
	"simulateServer": true
})

;
