/**
 * Created by vainio6 on 08/02/16.
 */

'use strict';

module.exports = function(RestService, $routeParams, $log, menuData, $location) {

    var scope = this;
    menuData.setItem("Create");

    this.params = $routeParams;

    this.product = {
        details:[]
    };

    this.initView = function(){

        if(scope.params.id) {
            RestService.getProduct(scope.params.id).then(function (data) {
                scope.product = data;
            });
        }
    }

    this.initView();

    this.addDetail = function(){
        scope.product.details.push({"dkey":"", "value": ""});
    }
    this.deleteDetail = function(detail){

        if(detail.id){
            RestService.deleteDetail(detail.id).then(function(data){
                scope.product.details = scope.product.details.filter(function(el) { return (el.dkey != detail.dkey && el.value != detail.value); });

            });
        }
        else{
            scope.product.details = scope.product.details.filter(function(el) { return (el.dkey != detail.dkey && el.value != detail.value); });
        }
    }

    this.submit = function(){

        // Clean up empty keys or values
        menuData.toggleSpinner();
        scope.product.details = scope.product.details.filter(function(detail){ return (detail.dkey!="" && detail.value !=""); });

        RestService.CreateOrUpdateProduct(scope.product).then(function(){
            menuData.toggleSpinner();
            $location.url("/productList");
        });
    }

};