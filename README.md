# README #

Product editor, a simple  client to browse and edit a product catalog

### FAST INSTALL: ###

1. Clone repo
2. Make sure you have node installed
3. npm install
4. Define your server setting in environmentConfig.json (root of the project)
5. Build Front-end application : gulp
6. The final app code should be now in dist-directory, deploy it as you like
7. Setup the product server also, not part of this package
8. You can also start application with command gulp serve (opens app in browser with browsersync)

### Features ###


### TODO AND BUGS ###

### Who do I talk to? ###

* Contact: janne.vainio (at) iki.fi
